Le site de l'association geeko, projet dans le cadre du TP  module Développement Web.
Le Media Query a changé, maintenant il se fait en fonction de la largeur de l'écran de la machine et non du navigateur. Cela permet au site de s'afficher correctement sur mobiles.


Avancement:
----FAIT----
-index.html:
    -header;
    -footer;
    -barre de navigation;
    -responsive design:
        -fichiers CSS adaptés.
    -création d'autres pages;
    volet de navigation fonctionnel.
    -contenu de index.html;
    -Overlay plus propre.

----RESTE A FAIRE----